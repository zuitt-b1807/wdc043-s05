public class Main {
    public static void main(String[] args) {
        User newUser = new User();
        newUser.setFirstName("Tee Jae");
        newUser.setLastName("Calinao");
        newUser.setAge(25);
        newUser.setAddress("Antipolo City");

        Course newCourse = new Course("Physics 101","Learn Physics 101",30,5000.0,"April 26","April 29",newUser );

        System.out.println("User's first name:");
        System.out.println(newUser.getFirstName());
        System.out.println("User's last name:");
        System.out.println(newUser.getLastName());
        System.out.println("User's age:");
        System.out.println(newUser.getAge());
        System.out.println("User's address:");
        System.out.println(newUser.getAddress());
        System.out.println("Course name:");
        System.out.println(newCourse.getName());
        System.out.println("Course description: ");
        System.out.println(newCourse.getDescription());
        System.out.println("Course seats: ");
        System.out.println(newCourse.getSeats());
        System.out.println("Course fee:");
        System.out.println(newCourse.getFee());
        System.out.println("Course instructor's first name:");
        System.out.println(newCourse.getCourseInstructorName());
    }

}